// ACTIVITY 1: QUIZ
/*
1.) Classes
2.) Uppercasing
3.) new
4.) instantiation
5.) constructor
*/

// ACTIVITY 1: FUNCTION CODING
class Student {

	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		grades.length == 4 && !grades.some(grade => grade > 100) ? this.grades = grades : this.grades = undefined;
	}

	// Method Chain " studentOne.login().listGrades().logout(); "
	login() {
			console.log(`${this.email} has logged in`);
			return this;
		}
		logout() {
			console.log(`${this.email} has logged out`);
			return this
		}
		listGrades(){
			console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
			return this;
		}
		computeAve() {
			let sum = 0;
			this.grades.forEach(grade => sum = sum + grade);
			//update property
			this.gradeAve = sum/4;
			//return object
			return this;
		}
		willPass() {
			this.passed = this.gradeAve >= 85 ? true : false
			return this;
		}

		willPassWithHonors() {
			this.passedWithHonors = this.isPassed && this.gradeAve >= 90 ? true : false;
			return this;
		}

}

//normal
let studentTestOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
console.log(studentTestOne);

//greater than 100
let studentTestTwo = new Student('John', 'john@mail.com', [101, 84, 78, 88]);
console.log(studentTestTwo);

// 3 grades only
let studentTestThree = new Student('John', 'john@mail.com', [84, 78, 88]);
console.log(studentTestThree);



let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
console.log(studentOne);

let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
console.log(studentTwo);

let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
console.log(studentThree);

let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
console.log(studentFour);



//ACTIVITY 2: QUIZ
/*
1.) No
2.) No
3.) Yes
4.) Getter and Setter
5.) this
*/
